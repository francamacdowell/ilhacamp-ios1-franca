class Solution {
    func exist(_ board: [[Character]], _ word: String) -> Bool {
        
        //if array board is empty return false 
        guard board.count > 0 && board[0].count > 0 else {
            return false
    }
        
    let hNumber = board.count
    let vNumber = board[0].count
    var visited = Array(count: hNumber, repeatedValue: Array(count: vNumber, repeatedValue: false))
        
    var wordContent = [Character](word.characters)
        
    for i in 0..<hNumber {
        for j in 0..<vNumber {
            if board[i][j] == wordContent[0] && _wordSearch(board, wordContent, hNumber,
            vNumber, i, j, &visited, 0) {
                return true
                }
            }
        }
        
        return false
    }
    
    //function to walk in list of lists and find or not the matched word
    private func _wordSearch(board: [[Character]], _ wordContent: [Character], _ hNumber: Int, _ vNumber: Int, _ i: Int, _ j: Int, _ visited: inout [[Bool]], _ index: Int) -> Bool {
        if index == wordContent.count {
            return true
        }
    
        guard i >= 0 && i < hNumber && j >= 0 && j < vNumber else {
            return false
        }
        guard !visited[i][j] && board[i][j] == wordContent[index] else {
            return false
        }
        
        visited[i][j] = true
        
        //recursive function's calls to walk through the list of lists
        
        if _wordSearch(board, wordContent, nNumber, vNumber, i + 1, j,
                       &visited, index + 1) || 
        _wordSearch(board, wordContent, nNumber, vNumber, i - 1, j,
                    &visited, index + 1) || 
        _wordSearch(board, wordContent, nNumber, vNumber, i, j + 1,
                    &visited, index + 1) || 
        _wordSearch(board, wordContent, nNumber, vNumber, i, j - 1,
                    &visited, index + 1) {
            
                return true
        }
        visited[i][j] = false
        
        return false
    }
}